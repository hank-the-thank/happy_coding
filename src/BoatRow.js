import React from 'react'
import Modal from 'react-responsive-modal'
import Card from '@material-ui/core/Card'
import CardActions from '@material-ui/core/CardActions'
import CardContent from '@material-ui/core/CardContent'
import CardMedia from '@material-ui/core/CardMedia'
import { Button, Grid } from '../node_modules/@material-ui/core'
import Typography from '@material-ui/core/Typography'
import axios from 'axios'

// import $ from 'jquery'

class BoatRow extends React.Component {

    viewBoat(){

        console.log('viewing boat')

        const url = 'https://www.trademe.co.nz/' + this.props.boat.name

        window.location.href = url

        this.stats = {
            name: '',
            img: '',
            worker: ''
        }
    }
    
    state = {

        openFirstModal: false,

        openSecondModal: false,
    }

    //update

    updateName = event => {

        this.setState({ name: event.target.value })
    }

    updateImg = event => {

        this.setState({img: event.target.value })
    }

    updateWorker = event => {

        this.setState({worker: event.target.value })
    }

    updateSubmit = () => {

        const name = this.state.name

        const img = this.state.img

        const worker = this.state.worker

        axios.put('http://localhost:3000/boats/'+ this.props.boat.id, {name, img, worker}).then(res => {

            console.log(res)

            console.log(res.data)
        })
    }

    //delete
    deleteObject = () => {

        axios.delete('http://localhost:3000/boats/'+ this.props.boat.id).then(res => {

            console.log(res)

            console.log(res.data)

            window.location.reload()
        })
    }

    //pop up windows setting

    onOpenFirstModal = () => {

        this.setState({ openFirstModal: true })
    }

    onCloseFirstModal = () => {

        this.setState({ openFirstModal: false })
    }

    onOpenSecondModal = () => {

        this.setState({ openSecondModal: true })
    }

    onCloseSecondModal = () => {

        this.setState({ openSecondModal: false })
    } 

    //React.Js render
    render() {

        const { openFirstModal, openSecondModal } = this.state

            return (

                <Grid container direction="column" justify="center" alignItems="center">

                    <Card>
                        
                        <CardMedia className='media'> 

                            <img alt='boat' src={this.props.boat.img} className='imgGrid'/> 

                        </CardMedia> 

                        <CardContent>

                            <Typography gutterBottom variant='headline' component='h2'>

                                {this.props.boat.name}

                            </Typography>

                            <Typography component='p'>

                                <p>Technician: {this.props.boat.worker}</p>

                            </Typography>

                        </CardContent> 

                        <CardContent> 

                            <CardActions>

                                <Button variant='outlined' color='primary' onClick={this.viewBoat.bind(this)} value='View'>

                                    Trademe

                                </Button>

                                <Button variant='outlined' color='primary' onClick={this.onOpenFirstModal}>

                                    Revise

                                </Button>

                                <Modal open={openFirstModal} onClose={this.onCloseFirstModal} center>

                                    <h2>{this.props.boat.name}</h2>

                                    <Button variant="outlined" color='primary' className='btn btn-action' onClick={this.onOpenSecondModal}>

                                        Modify

                                    </Button>

                                    <p width='50px'></p>

                                    <Button className='delete' variant="outlined" color='primary' onClick={this.deleteObject.bind(this)}>
                                   
                                        Delete
                                        
                                    </Button>

                                    <p>
                                        
                                        <img alt='boat' width='400px' src={this.props.boat.img}/>

                                    </p>

                                </Modal>

                                <Modal open={openSecondModal} onClose={this.onCloseSecondModal} center>

                                    <form onSubmit={this.updateSubmit}>

                                        <p></p>

                                        <input type='text' name='name' className='form' onChange={this.updateName.bind(this)} placeholder='Boat Name'/>

                                        <p></p>

                                        <input type='text' name='img' className='form' onChange={this.updateImg.bind(this)} placeholder='Put URL Here'/>

                                        <p></p>

                                        <input type='text' name='worker' className='form' onChange={this.updateWorker.bind(this)} placeholder='Assigned Worker'/>

                                        <p></p>

                                        <Button variant='contained' color='primary' type='submit'>
                                        
                                            Submit
                                        
                                        </Button>

                                    </form>

                                </Modal>

                            </CardActions>    

                        </CardContent>

                </Card>  
                
            </Grid>
        )
    }
}

export default BoatRow