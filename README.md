<h1>Start JSON</h1 >
<p>json-server db.json</p>

<h1>Start happy_coding</h1>
<p>npm start</p>
In the stage, terminal will ask you "do you want to try other port".
Which is normal, because the localhost:3000 is used by JSON server right now.
If you want to avoid it. Please type json-server --watch db.json --port 3004 (3004 is an random number).

<h3>Note</h3>
I put search bar in this project, but user has to look up lowercase and uppercase, becase the function hasn't implemented fully yet.
